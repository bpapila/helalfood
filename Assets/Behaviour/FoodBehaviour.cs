﻿using UnityEngine;
using System.Collections;

public class FoodBehaviour : MonoBehaviour {

	public int nutrition = 0;
	public int speedX = 0;
	public int speedY = 0;

	bool eaten = false;

	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody2D>().velocity = new Vector2 (speedX, speedY);
	}
	
	// Update is called once per frame
	void Update () {

		if (eaten) {
			Destroy (this.gameObject);
		}

		updatePosition ();

		if (isOutOfScreen()) {
			// Debug.Log ("Out of screen bounds. Destroying " + this.name + "(" + transform.position.x + "," + transform.position.y + ")");
			Destroy (this.gameObject);
		}
	}

	/**
	 * Updates the position of the object
	 */
	void updatePosition()
	{
		transform.Translate(GetComponent<Rigidbody2D>().velocity * Time.deltaTime);
	}

	/**
	 *	Checks if this is out of screen (only for y on the left side)
	 */
	bool isOutOfScreen()
	{
		// Get the screens left border and translate it to world coordinate
		Vector3 screenBoundary = Camera.main.ScreenToWorldPoint (
			new Vector3(0, 0, 0)
		);

		return transform.position.x < screenBoundary.x;
	}

	void OnTriggerEnter2D(Collider2D other) 
	{
		this.eaten = true;
		// Destroy (this.gameObject);
	}


}
