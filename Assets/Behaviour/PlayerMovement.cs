﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	public float speed = 3f;
	public int health = 10;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		MoveWithInput ();
	}

	/**
	 * Moves the player with player input
	 */
	void MoveWithInput()
	{
		float movex = Input.GetAxis ("Horizontal");
		float movey = Input.GetAxis ("Vertical");

		Vector2 velocity = GetComponent<Rigidbody2D> ().velocity;
		velocity.x = movex;
		velocity.y = movey; 
		GetComponent<Rigidbody2D> ().velocity = new Vector2(movex * speed, movey * speed);
	}

	/**
	 * Eat food with given foodbehaviour
	 */ 
	void Eat(FoodBehaviour food)
	{
		this.health += food.nutrition;
		Debug.Log ("Ate food: " + this.health);

	}

	void OnTriggerEnter2D(Collider2D other) {

		FoodBehaviour food = other.GetComponent<FoodBehaviour> ();

		if (food != null) {
			// Only eat food, no other objects
			this.Eat (food);
		}

	}
}
