﻿using UnityEngine;
using System.Collections;

/**
 * Responsible for spawning food 
 */
public class FoodSpawner : MonoBehaviour {

	public FoodBehaviour[] foodBehaviours;

	public float spawnCounterConfig = 1.5f;

	private float spawnCounter;

	// Use this for initialization
	void Start () 
	{
		spawnCounter = spawnCounterConfig;
	}
	
	// Update is called once per frame
	void Update () 
	{
		// Count back
		spawnCounter -= Time.deltaTime;

		if (spawnCounter <= float.Epsilon) {
			Spawn ();

			// set counter back to default
			spawnCounter = spawnCounterConfig;
		}
	}

	/**
	 * Spawns food
	 */
	private void Spawn ()
	{
		int index = Random.Range (0, foodBehaviours.Length);
		// Debug.Log ("Random index " + index);

		// Spawn food
		FoodBehaviour food = (FoodBehaviour) Instantiate (foodBehaviours[index]);

		// Add food as a child to spawner
		food.transform.SetParent (this.transform);

		// Create random y pos
		float yPos = generateRandomYPos();

		/* set position of created entity*/
		food.transform.position = new Vector2(
			this.transform.position.x, 
			yPos
		);

		/* Set speed */
		int randomSpeed = Random.Range (4, 5);
		food.speedX = - randomSpeed;

		// Debug.Log ("Cloned [" + food.transform.position + "] with speed " + randomSpeed);
	}

	/**
	 * Creates a random y position within screen boundaries
	 */
	float generateRandomYPos()
	{
		Vector3 screenPosition = Camera.main.ScreenToWorldPoint(
			new Vector3(
				0, 
				Random.Range(0 + 15, Screen.height - 10),  // with some offset, for foods not to appear ON screen boundaries
				0
			)
		);

		return screenPosition.y;
	}
}
